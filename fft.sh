#!/bin/bash

while getopts 'dh' OPTIONS; do
 case "$OPTIONS" in
   d) DELETE=true;;
   h) 
    echo "Usage:"
    echo "   [filename]		reads the [file structre] from a file and creates the directory tree"
    echo "   -d [filename] 	deletes the structure of given file"
    echo '''File Structure:
	example:
	file,folder,permission
	file1,folder_a,666
	file3_1,folder_b/folder_b1
	file2,folder_b
	file4_1,folder_b/folder_b1,o+x'''
    exit 0
    ;;
    ?)
     echo "Parameter is not defined! Usage fft.sh [-d] [-h] [filename] see -h"
    exit 0
    ;;
  esac
done
shift "$(($OPTIND -1))"

if [ -z $1 ];then
 echo "Please provide [filename]!"
 exit 0
fi

NUMOFLINES=$(wc -l < $1)

echo provided file: $1
echo number of lines: $NUMOFLINES

if [ $DELETE ];then
 for i in $(seq 1 $NUMOFLINES)
  do
   if [ $i -eq 1 ] 
    then
    IFS=',' read -r file dir permission
    echo "processing, parameters: " $file,$dir,$permission
    echo -------------------------------------------------
   elif [ $i -gt 1 ]
    then
    IFS=',' read -r file dir permission
    if [ -e $dir ]; then
     rm -rf $dir
     echo folder: $dir deleted
    else
     echo folder: $dir does not exist
    fi
    if [ -e $dir/$file ]; then
     rm $dir/$file
     echo file: $dir/$file deleted
    else
     echo file: $dir/$file does not exist
    fi
   echo -------------------------------------------------
   fi
  done < $1
elif [ -z $2 ];then
 for i in $(seq 1 $NUMOFLINES)
  do
   if [ $i -eq 1 ]
    then
    IFS=',' read -r file dir permission
    echo "processing, parameters: " $file,$dir,$permission
    echo -------------------------------------------------
   elif [ $i -gt 1 ]
    then
    IFS=',' read -r file dir permission
    if [ ! -e $dir ]; then
     mkdir -p $dir
     echo folder: $dir created
    else
     echo folder: $dir already exists
    fi
    if [ ! -e $dir/$file ]; then
     touch $dir/$file
     echo file: $dir/$file created
    else
     echo file: $dir/$file already exists
    fi
    if [ ! -z $permission ]; then
     chmod $permission $dir/$file
     echo permission: $permission set on $dir/$file
    fi
   echo -------------------------------------------------
   fi
  done < $1
fi
echo "done!"
  
